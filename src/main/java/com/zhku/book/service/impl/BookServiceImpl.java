package com.zhku.book.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhku.book.pojo.Book;
import com.zhku.book.pojo.resp.BookResp;
import com.zhku.book.service.BookService;
import com.zhku.book.mapper.BookMapper;
import org.springframework.stereotype.Service;

/**
* @author ZoZou02
* @description 针对表【book】的数据库操作Service实现
* @createDate 2024-03-14 16:48:07
*/
@Service
public class BookServiceImpl extends ServiceImpl<BookMapper, Book>
    implements BookService{
    @Override
    public IPage<BookResp> findBookAndCategory(IPage<BookResp> page, Integer categoryId) {
        return this.baseMapper.findBookAndCategory(page,categoryId);
    }
}




