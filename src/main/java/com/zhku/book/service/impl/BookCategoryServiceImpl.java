package com.zhku.book.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhku.book.pojo.BookCategory;
import com.zhku.book.service.BookCategoryService;
import com.zhku.book.mapper.BookCategoryMapper;
import org.springframework.stereotype.Service;

/**
* @author ZoZou02
* @description 针对表【book_category】的数据库操作Service实现
* @createDate 2024-03-14 16:48:07
*/
@Service
public class BookCategoryServiceImpl extends ServiceImpl<BookCategoryMapper, BookCategory>
    implements BookCategoryService{

}




