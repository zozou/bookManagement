package com.zhku.book.service;

import com.zhku.book.pojo.BookCategory;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author ZoZou02
* @description 针对表【book_category】的数据库操作Service
* @createDate 2024-03-14 16:48:07
*/
public interface BookCategoryService extends IService<BookCategory> {

}
