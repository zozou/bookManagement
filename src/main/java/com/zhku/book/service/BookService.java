package com.zhku.book.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.zhku.book.pojo.Book;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zhku.book.pojo.resp.BookResp;

/**
* @author ZoZou02
* @description 针对表【book】的数据库操作Service
* @createDate 2024-03-14 16:48:07
*/
public interface BookService extends IService<Book> {
    IPage<BookResp> findBookAndCategory(IPage<BookResp> page, Integer categoryId);

}
