package com.zhku.book.mapper;

import com.zhku.book.pojo.BookCategory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author ZoZou02
* @description 针对表【book_category】的数据库操作Mapper
* @createDate 2024-03-14 16:48:07
* @Entity com.zhku.book.pojo.BookCategory
*/
public interface BookCategoryMapper extends BaseMapper<BookCategory> {

}




