package com.zhku.book.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.zhku.book.pojo.Book;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhku.book.pojo.resp.BookResp;

/**
* @author ZoZou02
* @description 针对表【book】的数据库操作Mapper
* @createDate 2024-03-14 16:48:07
* @Entity com.zhku.book.pojo.Book
*/
public interface BookMapper extends BaseMapper<Book> {
    IPage<BookResp> findBookAndCategory(IPage<BookResp> page, Integer categoryId);
}




