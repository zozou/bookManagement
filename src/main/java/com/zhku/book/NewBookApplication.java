package com.zhku.book;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.zhku.book.mapper")
public class NewBookApplication {

    public static void main(String[] args) {
        SpringApplication.run(NewBookApplication.class, args);
    }

}
