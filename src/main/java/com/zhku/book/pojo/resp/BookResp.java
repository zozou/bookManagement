package com.zhku.book.pojo.resp;

import com.zhku.book.pojo.Book;
import lombok.Data;

/**
 * 主要用来封闭分页查询的结果数据的
 */
@Data
public class BookResp extends Book {

    private String categoryName;
}
